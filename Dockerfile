FROM ubuntu:18.04

RUN apt-get update -qq \
    && apt install -y --install-recommends clamav \
    && freshclam --no-warnings