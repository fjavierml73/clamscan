# clamscan

GitLab job to scan repository for known malware using [ClamAV](https://www.clamav.net/about)

Installs clamav, updates signatures, and scans all files in repository
for [trojans, viruses, malware & other malicious threats](https://www.clamav.net/)

Template in `/template/clamscan.gitlab-ci.yml`:

```yml
clamscan:
  image: alpine:latest
  script:
  - apk update &&  apk add clamav clamav-libunrar
  - freshclam --show-progress --no-warnings
  - clamscan -zira | tee scan-results.txt
  allow_failure: true
  artifacts:
    when: on_failure
    paths:
    - scan-results.txt
```

To [include:remote](https://docs.gitlab.com/ee/ci/yaml/#single-string-or-array-of-multiple-values) this template:

```yaml
include:
  - remote: 'https://gitlab.com/greg/clamscan/-/raw/master/templates/clamscan.gitlab-ci.yml'
```

Sample output:

```shell
/builds/user/project/eicar.com.txt: Eicar-Test-Signature FOUND
/builds/user/project/eicar.com.txt!(0): Eicar-Test-Signature FOUND
/builds/user/project/eicarcom2.zip: Eicar-Test-Signature FOUND
/builds/user/project/eicarcom2.zip!ZIP:eicar_com.zip!(2)ZIP:eicar.com: Eicar-Test-Signature FOUND
/builds/user/project/eicar_com.zip: Eicar-Test-Signature FOUND
/builds/user/project/eicar_com.zip!(1)ZIP:eicar.com: Eicar-Test-Signature FOUND
/builds/user/project/eicar.com: Eicar-Test-Signature FOUND
/builds/user/project/eicar.com!(0): Eicar-Test-Signature FOUND

----------- SCAN SUMMARY -----------
Known viruses: 6295218
Engine version: 0.100.3
Scanned directories: 1
Scanned files: 7
Infected files: 4
Data scanned: 0.00 MB
Data read: 0.00 MB (ratio 0.00:1)
Time: 48.769 sec (0 m 48 s)
```

Created as proof of concept for [gitlab-ee/issues/6721](https://gitlab.com/gitlab-org/gitlab-ee/issues/6721) and [gitlab-ce/issues/53560](https://gitlab.com/gitlab-org/gitlab-ce/issues/53560)

Malware test files from [eicar.org](https://www.eicar.org/)
